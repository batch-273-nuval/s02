//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//create student one
/*let studentOneName = 'John';
let studentOneEmail = 'john@mail.com';
let studentOneGrades = [89, 84, 78, 88];

//create student two
let studentTwoName = 'Joe';
let studentTwoEmail = 'joe@mail.com';
let studentTwoGrades = [78, 82, 79, 85];

//create student three
let studentThreeName = 'Jane';
let studentThreeEmail = 'jane@mail.com';
let studentThreeGrades = [87, 89, 91, 93];

//create student four
let studentFourName = 'Jessie';
let studentFourEmail = 'jessie@mail.com';
let studentFourGrades = [91, 89, 92, 93];*/

//actions that students may perform will be lumped together
/*function login(email){
    console.log(`${email} has logged in`);
}

function logout(email){
    console.log(`${email} has logged out`);
}

function listGrades(grades){
    grades.forEach(grade => {
        console.log(grade);
    })
}*/

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects



//Use an object literal
//ENCAPSULATION
// Spaghetti code - code that is poorly organized that it becomes almost mpossible to work with

let studentOne = {
    name: 'John',
    email: 'john@mail.com',
    grades: [89, 84, 78, 88],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum / 4;
    },
    willPass() {
         if (this.computeAve() >= 85) {
           return true; 
        } else {
        return false;
        }
    },
    willPassWithHonors() {
        if (this.willPass() === true && this.computeAve() >= 90) {
            return true;
        } else if (this.willPass() === true && this.computeAve() >= 85 && this.computeAve() < 90) {
            return false;
        } else {
            return undefined;
        }
    }

/*  willPassWithHonors() {
        return (this.willPass() && this.computerAve() >= 90) ? true : false;
}*/

}

console.log(`student one's name is ${studentOne.name}`);
console.log(`student one's email is ${studentOne.email}`);
console.log(`student one's quarterly grade average are ${studentOne.grades}`);  






/*
Quiz
    1. What is the term given to unorganized code that's very hard to work with?
        - Spaghetti Code

    2. How are object literals written in JS?
        - By using Curly Braces or Brackets {}

    3. What do you call the concept of organizing information and functionality to belong to an object?
        - Encapsulation

    4. If studentOne has a method named enroll(), how would you invoke it?
        - By using the "this" method

    5. True or False: Objects can have objects as properties.
        - True

    6. What is the syntax in creating key-value pairs?
        - key : value

    7. True or False: A method can have no parameters and still work.
        - True

    8. True or False: Arrays can have objects as elements.
        - True

    9. True or False: Arrays are objects.
        - Special Type of Objects, True

    10. True or False: Objects can have arrays as properties.
        - True

*/

// Function Coding:

// 1. 2. 3. 4.

let studentTwo = {
    name: 'Joe',
    email: 'joe@mail.com',
    grades: [78, 82, 79, 85],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum / 4;
    },
    willPass() {
         if (this.computeAve() >= 85) {
           return true; 
        } else {
        return false;
        }
    },
    willPassWithHonors() {
        if (this.willPass() === true && this.computeAve() >= 90) {
            return true;
        } else if (this.willPass() === true && this.computeAve() >= 85 && this.computeAve() < 90) {
            return false;
        } else {
            return undefined;
        }
    }
}

let studentThree = {
    name: 'Jane',
    email: 'jane@mail.com',
    grades: [87, 89, 91, 93],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum / 4;
    },
    willPass() {
         if (this.computeAve() >= 85) {
           return true; 
        } else {
        return false;
        }
    },
    willPassWithHonors() {
        if (this.willPass() === true && this.computeAve() >= 90) {
            return true;
        } else if (this.willPass() === true && this.computeAve() >= 85 && this.computeAve() < 90) {
            return false;
        } else {
            return undefined;
        }
    }
}

let studentFour = {
    name: 'Jessie',
    email: 'jessie@mail.com',
    grades: [91, 89, 92, 93],

    login() {
        console.log(`${this.email} has logged in`);
    },
    logout() {
        console.log(`${this.email} has logged out`);
    },
    listGrades() {
       console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`); 
    },
    computeAve() {
        let sum = 0;
        this.grades.forEach(grade => sum = sum + grade);
        return sum / 4;
    },
    willPass() {
         if (this.computeAve() >= 85) {
           return true; 
        } else {
        return false;
        }
    },
    willPassWithHonors() {
        if (this.willPass() === true && this.computeAve() >= 90) {
            return true;
        } else if (this.willPass() === true && this.computeAve() >= 85 && this.computeAve() < 90) {
            return false;
        } else {
            return undefined;
        }
    }
}

// 5. 6. 7.

let classOf1A = {
    students : [studentOne, studentTwo, studentThree, studentFour],
    countHonorStudents() {
    let count = 0;
    this.students.forEach((honor) => {
        if (honor.willPassWithHonors()) {
            count++;
            }
        });
        return count;
    },
    honorsPercentage() {
        return (this.countHonorStudents() / this.students.length) * 100;
    }
};
